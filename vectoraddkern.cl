__kernel void vector_add(__global float* src_a, __global float* src_b,
			 __global float* res, int n)
{
	int i = get_global_id(0);

	if (i < n)
		res[i] = src_a[i] + src_b[i];
}
