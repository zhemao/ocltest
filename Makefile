LDFLAGS=-L/opt/intel/opencl-sdk -lOpenCL
CFLAGS=-Wall -g

all: matmult vectoradd

matmult: matmult.o util.o
	$(CC) $(LDFLAGS) matmult.o util.o -o $@

vectoradd: vectoradd.o util.o
	$(CC) $(LDFLAGS) vectoradd.o util.o -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $<

.PHONY: clean

clean:
	rm -f matmult vectoradd *.o
