#include <CL/opencl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "util.h"

#define N 1024
#define CL_MEM_READ_ONLY_COPY (CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR)

void genLine(float *line, float m, float b, int n)
{
	int i;

	for (i = 0; i < n; i++) {
		line[i] = m * i + b;
	}
}

cl_int loadAndBuildProgram(cl_context context, cl_program *program, 
				cl_device_id *device,
				const char *filepath)
{
	size_t len;
	cl_int error;
	char *source = file_contents(filepath, &len);

	if (source == NULL)
		return errno;
	
	*program = clCreateProgramWithSource(context, 1, 
				(const char **) &source, &len, &error);
	if (error != CL_SUCCESS) {
		free(source);
		return error;
	}

	error = clBuildProgram(*program, 1, device, NULL, NULL, NULL); 
	if (error != CL_SUCCESS) {
		free(source);
		return error;
	}

	free(source);

	return CL_SUCCESS;
}

int check_result(float *res, float *expected, int n)
{
	int i;

	for (i = 0; i < n; i++) {
		if (res[i] != expected[i])
			return 0;
	}

	return 1;
}

int main(int argc, char *argv[])
{
	cl_int error = 0;
	cl_uint num_platforms;
	cl_platform_id platform;
	cl_context context;
	cl_command_queue queue;
	cl_device_id device;
	cl_program program;
	cl_kernel kernel;
	cl_event event;

	int size = N * sizeof(float);
	int nmemb = N;
	size_t global_ws;
	size_t local_ws;
	float src_a_h[N];
	float src_b_h[N];
	float res_h[N];
	float expected[N];

	cl_mem src_a_d;
	cl_mem src_b_d;
	cl_mem res_d;

	genLine(src_a_h, 0.5, 0, N);
	genLine(src_b_h, 0, 2, N);
	genLine(expected, 0.5, 2, N);

	error = clGetPlatformIDs(1, &platform, &num_platforms);
	if (error != CL_SUCCESS) {
		printOclError("clGetPlatformIDs", error);
		return error;
	}

	error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_DEFAULT, 1, &device, NULL);
	if (error != CL_SUCCESS) {
		printOclError("clGetDeviceIDs", error);
		return error;
	}

	context = clCreateContext(0, 1, &device, NULL, NULL, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateContext", error);
		return error;
	}
	
	queue = clCreateCommandQueue(context, device, 0, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateCommandQueue", error);
		return error;
	}

	src_a_d = clCreateBuffer(context, CL_MEM_READ_ONLY_COPY,
				size, src_a_h, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateBuffer src_a_d", error);
		return error;
	}
	src_b_d = clCreateBuffer(context, CL_MEM_READ_ONLY_COPY,
				size, src_b_h, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateBuffer src_b_d", error);
		return error;
	}
	res_d = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
				size, NULL, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateBuffer res_d", error);
		return error;
	}

	error = loadAndBuildProgram(context, &program, &device, 
					"vectoraddkern.cl");
	if (error != CL_SUCCESS) {
		printBuildLog(stderr, program, device);
		return error;
	} 
	
	kernel = clCreateKernel(program, "vector_add", &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateKernel", error);
		return error;
	}

	error = clSetKernelArg(kernel, 0, sizeof(src_a_d), &src_a_d);
	error |= clSetKernelArg(kernel, 1, sizeof(src_b_d), &src_b_d);
	error |= clSetKernelArg(kernel, 2, sizeof(res_d), &res_d);
	error |= clSetKernelArg(kernel, 3, sizeof(nmemb), &nmemb);

	if (error != CL_SUCCESS) {
		printOclError("clSetKernelArg", error);
		return error;
	}

	global_ws = 1024;
	error = clGetKernelWorkGroupInfo(kernel, device, 
					CL_KERNEL_WORK_GROUP_SIZE,
					sizeof(local_ws), &local_ws, NULL);

	if (error != CL_SUCCESS) {
		printOclError("clGetKernelWorkGroupInfo", error);
		return error;
	}

	error = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, 
					&global_ws, &local_ws,
					0, NULL, &event);
	
	if (error != CL_SUCCESS) {
		printOclError("clEnqueueNDRangeKernel", error);
		return error;
	}

	error = clEnqueueReadBuffer(queue, res_d, CL_TRUE, 0, size, res_h, 
					1, &event, NULL);

	if (error != CL_SUCCESS) {
		printOclError("clEnqueueReadBuffer", error);
		return error;
	}

	if (!check_result(res_h, expected, N)) {
		fprintf(stderr, "Incorrect result\n");
		return error;
	}

	clReleaseKernel(kernel);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
	clReleaseMemObject(src_a_d);
	clReleaseMemObject(src_b_d);
	clReleaseMemObject(res_d);

	return 0;
}
