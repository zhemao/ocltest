__kernel void mat_mult(__global float *src_a, __global float *src_b,
		       __global float *res, int m, int n, int r)
{
	int row = get_global_id(0);
	int col = get_global_id(1);
	int i, sum = 0;

	if (row >= m || col >= r)
		return;

	for (i = 0; i < n; i++)
		sum += src_a[row * n + i] * src_b[i * r + col];
	
	res[row * r + col] = sum;
}
