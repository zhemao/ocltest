//from http://www.songho.ca/opengl/gl_vbo.html

#ifndef ADVCL_UTIL_H_INCLUDED
#define ADVCL_UTIL_H_INCLUDED

char *file_contents(const char *filename, size_t *length);

const char* oclErrorString(cl_int error);

void printBuildLog(FILE *fp, cl_program program, cl_device_id device);
void printOclError(const char *tag, cl_int error);

#endif
