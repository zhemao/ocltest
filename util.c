#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <CL/opencl.h>

#include "util.h"

char *file_contents(const char *filename, size_t *length)
{
	FILE *f = fopen(filename, "r");
	char *buffer;

	if (f == NULL)
		return NULL;

	if (fseek(f, 0, SEEK_END) < 0)
		goto outferror;

	if ((*length = ftell(f)) < 0)
		goto outferror;

	if (fseek(f, 0, SEEK_SET) < 0)
		goto outferror;

	buffer = malloc(*length+1);

	if (buffer == NULL)
		goto outferror;

	*length = fread(buffer, 1, *length, f);

	if (*length <= 0) {
		free(buffer);
		goto outferror;
	}

	fclose(f);
	
	buffer[*length] = '\0';

	return buffer;

outferror:
	fclose(f);
	return NULL;
}




// Helper function to get error string
// *********************************************************************
const char* oclErrorString(cl_int error)
{
    static const char* errorString[] = {
        "CL_SUCCESS",
        "CL_DEVICE_NOT_FOUND",
        "CL_DEVICE_NOT_AVAILABLE",
        "CL_COMPILER_NOT_AVAILABLE",
        "CL_MEM_OBJECT_ALLOCATION_FAILURE",
        "CL_OUT_OF_RESOURCES",
        "CL_OUT_OF_HOST_MEMORY",
        "CL_PROFILING_INFO_NOT_AVAILABLE",
        "CL_MEM_COPY_OVERLAP",
        "CL_IMAGE_FORMAT_MISMATCH",
        "CL_IMAGE_FORMAT_NOT_SUPPORTED",
        "CL_BUILD_PROGRAM_FAILURE",
        "CL_MAP_FAILURE",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "",
        "CL_INVALID_VALUE",
        "CL_INVALID_DEVICE_TYPE",
        "CL_INVALID_PLATFORM",
        "CL_INVALID_DEVICE",
        "CL_INVALID_CONTEXT",
        "CL_INVALID_QUEUE_PROPERTIES",
        "CL_INVALID_COMMAND_QUEUE",
        "CL_INVALID_HOST_PTR",
        "CL_INVALID_MEM_OBJECT",
        "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
        "CL_INVALID_IMAGE_SIZE",
        "CL_INVALID_SAMPLER",
        "CL_INVALID_BINARY",
        "CL_INVALID_BUILD_OPTIONS",
        "CL_INVALID_PROGRAM",
        "CL_INVALID_PROGRAM_EXECUTABLE",
        "CL_INVALID_KERNEL_NAME",
        "CL_INVALID_KERNEL_DEFINITION",
        "CL_INVALID_KERNEL",
        "CL_INVALID_ARG_INDEX",
        "CL_INVALID_ARG_VALUE",
        "CL_INVALID_ARG_SIZE",
        "CL_INVALID_KERNEL_ARGS",
        "CL_INVALID_WORK_DIMENSION",
        "CL_INVALID_WORK_GROUP_SIZE",
        "CL_INVALID_WORK_ITEM_SIZE",
        "CL_INVALID_GLOBAL_OFFSET",
        "CL_INVALID_EVENT_WAIT_LIST",
        "CL_INVALID_EVENT",
        "CL_INVALID_OPERATION",
        "CL_INVALID_GL_OBJECT",
        "CL_INVALID_BUFFER_SIZE",
        "CL_INVALID_MIP_LEVEL",
        "CL_INVALID_GLOBAL_WORK_SIZE",
    };

    const int errorCount = sizeof(errorString) / sizeof(errorString[0]);

    const int index = -error;

    return (index >= 0 && index < errorCount) ? errorString[index] : "";

}

void printBuildLog(FILE *fp, cl_program program, cl_device_id device)
{
	char *build_log;
	size_t len;

	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG, 
				0, NULL, &len);
	build_log = malloc(len + 1);
	if (build_log == NULL)
		return;
	
	clGetProgramBuildInfo(program, device, CL_PROGRAM_BUILD_LOG,
				len, build_log, NULL);
	build_log[len] = '\0';

	fprintf(fp, "%s", build_log);

	free(build_log);
}

inline void printOclError(const char *tag, cl_int error)
{
	fprintf(stderr, "%s: %s\n", tag, oclErrorString(error));
}
