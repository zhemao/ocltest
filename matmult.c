#include <CL/opencl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "util.h"

#define CL_MEM_READ_ONLY_COPY (CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR)

cl_int loadAndBuildProgram(cl_context context, cl_program *program, 
				cl_device_id *device,
				const char *filepath)
{
	size_t len;
	cl_int error;
	char *source = file_contents(filepath, &len);

	if (source == NULL)
		return errno;
	
	*program = clCreateProgramWithSource(context, 1, 
				(const char **) &source, &len, &error);
	if (error != CL_SUCCESS) {
		free(source);
		return error;
	}

	error = clBuildProgram(*program, 1, device, NULL, NULL, NULL); 
	if (error != CL_SUCCESS) {
		free(source);
		return error;
	}

	free(source);

	return CL_SUCCESS;
}

int check_result(float *res, float *expected, int n)
{
	int i;

	for (i = 0; i < n; i++) {
		if (res[i] != expected[i])
			return 0;
	}

	return 1;
}

int main(int argc, char *argv[])
{
	cl_int error = 0;
	cl_uint num_platforms;
	cl_platform_id platform;
	cl_context context;
	cl_command_queue queue;
	cl_device_id device;
	cl_program program;
	cl_kernel kernel;
	cl_event event;

	const int m = 3;
	const int n = 2;
	const int r = 2;
	size_t global_ws[2];
	size_t local_ws[2];

	float src_a_h[6] = {1, 0, 2, 5, 4, 3};
	float src_b_h[4] = {2, 0, 0, 2};
	float res_h[m * r];
	float expected[6];

	cl_mem src_a_d;
	cl_mem src_b_d;
	cl_mem res_d;

	error = clGetPlatformIDs(1, &platform, &num_platforms);
	if (error != CL_SUCCESS) {
		printOclError("clGetPlatformIDs", error);
		return error;
	}

	error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_DEFAULT, 1, &device, NULL);
	if (error != CL_SUCCESS) {
		printOclError("clGetDeviceIDs", error);
		return error;
	}

	context = clCreateContext(0, 1, &device, NULL, NULL, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateContext", error);
		return error;
	}
	
	queue = clCreateCommandQueue(context, device, 0, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateCommandQueue", error);
		return error;
	}

	src_a_d = clCreateBuffer(context, CL_MEM_READ_ONLY_COPY,
				m * n * sizeof(float), src_a_h, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateBuffer src_a_d", error);
		return error;
	}
	src_b_d = clCreateBuffer(context, CL_MEM_READ_ONLY_COPY,
				n * r * sizeof(float), src_b_h, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateBuffer src_b_d", error);
		return error;
	}
	res_d = clCreateBuffer(context, CL_MEM_WRITE_ONLY,
				m * r * sizeof(float), NULL, &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateBuffer res_d", error);
		return error;
	}

	error = loadAndBuildProgram(context, &program, &device, 
					"matmultkern.cl");
	if (error != CL_SUCCESS) {
		printBuildLog(stderr, program, device);
		return error;
	} 
	
	kernel = clCreateKernel(program, "mat_mult", &error);
	if (error != CL_SUCCESS) {
		printOclError("clCreateKernel", error);
		return error;
	}

	error = clSetKernelArg(kernel, 0, sizeof(src_a_d), &src_a_d);
	error |= clSetKernelArg(kernel, 1, sizeof(src_b_d), &src_b_d);
	error |= clSetKernelArg(kernel, 2, sizeof(res_d), &res_d);
	error |= clSetKernelArg(kernel, 3, sizeof(m), &m);
	error |= clSetKernelArg(kernel, 4, sizeof(n), &n);
	error |= clSetKernelArg(kernel, 5, sizeof(r), &r);

	if (error != CL_SUCCESS) {
		printOclError("clSetKernelArg", error);
		return error;
	}

	global_ws[0] = m;
	global_ws[1] = n;

	local_ws[0] = m;
	local_ws[1] = n;


	error = clEnqueueNDRangeKernel(queue, kernel, 2, NULL, 
					global_ws, local_ws,
					0, NULL, &event);
	
	if (error != CL_SUCCESS) {
		printOclError("clEnqueueNDRangeKernel", error);
		return error;
	}

	error = clEnqueueReadBuffer(queue, res_d, CL_TRUE, 0, m * r * sizeof(float),
					res_h, 1, &event, NULL);

	if (error != CL_SUCCESS) {
		printOclError("clEnqueueReadBuffer", error);
		return error;
	}
	
	int i;
	for (i = 0; i < m * r; i++) {
		expected[i] = src_a_h[i] * 2;
	}
	if (!check_result(res_h, expected, m * r)) {
		fprintf(stderr, "Incorrect result\n");
		return error;
	}

	clReleaseKernel(kernel);
	clReleaseCommandQueue(queue);
	clReleaseContext(context);
	clReleaseMemObject(src_a_d);
	clReleaseMemObject(src_b_d);
	clReleaseMemObject(res_d);

	return 0;
}
